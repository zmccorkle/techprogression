package me.zebmccorkle.techprogression;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityTNTPrimed;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ExplosionWand extends Item {
	public static final String unlocalizedName = "ExplosionWand";
	
	public ExplosionWand() {
		super();
		
		this.setUnlocalizedName(TechProgression.MODID + "_" + this.unlocalizedName);
		this.setCreativeTab(CreativeTabs.tabTools);
		this.setMaxStackSize(64);
	}
	
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer) {
		par2World.createExplosion(par3EntityPlayer, par3EntityPlayer.posX, par3EntityPlayer.posY, par3EntityPlayer.posZ, 6.5F, true);
		par1ItemStack.stackSize--;
		par3EntityPlayer.capabilities.allowFlying = true;
		par3EntityPlayer.attackEntityFrom(DamageSource.magic, 1);
		
		return par1ItemStack;
	}
}

package me.zebmccorkle.techprogression;

import net.minecraft.block.material.Material;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ItemManager {
	public static ExplosionWand explosionWand;
	public static Thermometer thermometer;
	public static MechanicalDrill mechanicalDrill;
	public static DiamondMechanicalDrill diamondMechanicalDrill;
	public static WaterBoiler waterBoiler;
	public static Fluid steam;
	public static BlockSteam steamBlock;
	
	public static void mainRegistry() {
		initializeItem();
		registerItem();
	}
	public static void initializeItem() {
		explosionWand = new ExplosionWand();
		thermometer = new Thermometer();
		mechanicalDrill = new MechanicalDrill(TechProgression.mechanicalMaterial);
		diamondMechanicalDrill = new DiamondMechanicalDrill(ToolMaterial.EMERALD);
		waterBoiler = new WaterBoiler();
		steam = new Fluid("steam");
		steam.setLuminosity(2);
		steam.setDensity(500);
		steam.setViscosity(500);
		steam.setGaseous(true);
		steam.setUnlocalizedName(TechProgression.MODID + "_steam");
	}
	public static void registerItem() {
		GameRegistry.registerItem(explosionWand, explosionWand.unlocalizedName);
		GameRegistry.registerItem(thermometer, thermometer.unlocalizedName);
		GameRegistry.registerItem(mechanicalDrill, mechanicalDrill.unlocalizedName);
		GameRegistry.registerItem(diamondMechanicalDrill, diamondMechanicalDrill.unlocalizedName);
		GameRegistry.registerBlock(waterBoiler, waterBoiler.unlocalizedName);
		FluidRegistry.registerFluid(steam);
		steamBlock = new BlockSteam(steam, Material.water);
		GameRegistry.registerBlock(steamBlock, steamBlock.unlocalizedName);
	}
}

package me.zebmccorkle.techprogression;

import net.minecraft.block.material.Material;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;

public class BlockSteam extends BlockFluidClassic {
	public static final String unlocalizedName = "steam";
	
	public BlockSteam(Fluid fluid, Material material) {
		super(fluid, material);
		this.setUnlocalizedName(TechProgression.MODID + "_" + unlocalizedName);
	}

}

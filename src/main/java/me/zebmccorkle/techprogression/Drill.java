package me.zebmccorkle.techprogression;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class Drill extends ItemPickaxe {
	protected Drill(ToolMaterial p_i45347_1_) {
		super(p_i45347_1_);
	}

	@Override
	public void onUsingTick(ItemStack stack, EntityPlayer player, int count) {
		System.out.println("using");
		if (stack.getItemDamage() >= 0) {
			stack.setItem(Items.stick);
			stack.setItemDamage(0);
		}
	}
	public boolean onBlockDestroyed(ItemStack p_150894_1_, World p_150894_2_, Block p_150894_3_, int p_150894_4_, int p_150894_5_, int p_150894_6_, EntityLivingBase p_150894_7_) {
		System.out.println("thing");
		if (p_150894_1_.getItemDamage() >= 0) {
			p_150894_1_.setItem(Items.stick);
			p_150894_1_.setItemDamage(0);
		}
		return super.onBlockDestroyed(p_150894_1_, p_150894_2_, p_150894_3_, null, p_150894_7_);
	}
}

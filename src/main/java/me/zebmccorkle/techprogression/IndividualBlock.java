package me.zebmccorkle.techprogression;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;

/**
 * Created by Zeb on 7/21/2015.
 */
public class IndividualBlock {
    public World world;
    public BlockPos pos;
    public IBlockState blockState;
    public Block blockType;

    public IndividualBlock(World world, BlockPos pos) {
        this.world = world;
        this.pos = pos;
        this.blockState = world.getBlockState(pos);
        this.blockType = blockState.getBlock();
    }
    
    public boolean is(String unlocalizedName) {
    	return this.blockType.getUnlocalizedName().endsWith(unlocalizedName);
    }
}

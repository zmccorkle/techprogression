package me.zebmccorkle.techprogression;

import net.minecraft.block.Block;
import net.minecraft.block.BlockFurnace;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.BlockTorch;
import net.minecraft.init.Blocks;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;

/**
 * Energy abstraction
 */
public class EnergyNet implements IHeatProvider {
    public double ROOM_TEMPERATURE = 293.15;
    public double RESISTANCE_MULT  = 0.75;

	public EnergyNet() {
	}

    /**
     * Gets the temperature of a block
     *
     * @param world World the block is in
     * @param pos   Position of the block
     * @return Temperature in Kelvin
     */
    public double getTemperature(World world, BlockPos pos) {
        return getTemperature(new IndividualBlock(world, pos));
    }

    /**
     * Gets the temperature of a block
     *
     * @param indblock Block
     * @return Temperature in Kelvin
     */
    public double getTemperature(IndividualBlock indblock) {
        Block block = indblock.blockType;
        if (block instanceof BlockFurnace) {
            // 1811 Kelvin is the melting point of Iron (which furnaces smelt). Sounds good.
            return block == Blocks.lit_furnace ? 1811f : ROOM_TEMPERATURE;
        } else if (block instanceof IHeatProvider) {
            IHeatProvider heatProvider = (IHeatProvider) block;
            return heatProvider.getTemperature(indblock);
        } else if (block instanceof BlockTorch) {
        	return 473.15;
        } else {
        	IndividualBlock above = new IndividualBlock(indblock.world, new BlockPos(indblock.pos.getX(), indblock.pos.getY()+1, indblock.pos.getZ()));
        	IndividualBlock below = new IndividualBlock(indblock.world, new BlockPos(indblock.pos.getX(), indblock.pos.getY()-1, indblock.pos.getZ()));
        	IndividualBlock north = new IndividualBlock(indblock.world, new BlockPos(indblock.pos.getX(), indblock.pos.getY(), indblock.pos.getZ()-1));
        	IndividualBlock south = new IndividualBlock(indblock.world, new BlockPos(indblock.pos.getX(), indblock.pos.getY(), indblock.pos.getZ()+1));
        	IndividualBlock east  = new IndividualBlock(indblock.world, new BlockPos(indblock.pos.getX()+1, indblock.pos.getY(), indblock.pos.getZ()));
        	IndividualBlock west  = new IndividualBlock(indblock.world, new BlockPos(indblock.pos.getX()-1, indblock.pos.getY(), indblock.pos.getZ()));
        	if (above.is("tile.lava") || below.is("tile.lava") || north.is("tile.lava") || south.is("tile.lava") || east.is("tile.lava") || west.is("tile.lava")) {
        		return 1273.15 * RESISTANCE_MULT;
        	} else {
        		return ROOM_TEMPERATURE;
        	}
        }
    }
}

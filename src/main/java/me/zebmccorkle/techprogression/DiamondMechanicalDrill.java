package me.zebmccorkle.techprogression;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemTool;
import net.minecraft.block.Block;

public class DiamondMechanicalDrill extends Drill {
	public static String unlocalizedName = "DiamondMechanicalDrill";
	
	protected DiamondMechanicalDrill(ToolMaterial material) {
		super(material);
		
		this.setUnlocalizedName(TechProgression.MODID + "_" + this.unlocalizedName);
		this.setMaxDamage(232);
		this.setCreativeTab(CreativeTabs.tabTools);
	}
}

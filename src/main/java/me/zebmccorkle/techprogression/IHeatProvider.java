package me.zebmccorkle.techprogression;

/**
 * Provider of heat.
 */
public interface IHeatProvider {
	/**
	 * Get the temperature of the block
	 *
	 * @param indblock Block
	 * @return Temperature in Kelvin
	 */
	double getTemperature(IndividualBlock indblock);
}

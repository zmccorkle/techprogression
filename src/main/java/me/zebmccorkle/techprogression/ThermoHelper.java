package me.zebmccorkle.techprogression;

/**
 * Created by Zeb on 7/21/2015.
 */
public class ThermoHelper {
    public static double fToK(double f) {
        return (f + 459.67) * (5 / 9);
    }

    public static double cToK(double c) {
        return c + 273.15;
    }

    public static double kToF(double k) {
        return (k * (9 / 5)) - 459.67;
    }

    public static double kToC(double k) {
        return k - 273.15;
    }
}

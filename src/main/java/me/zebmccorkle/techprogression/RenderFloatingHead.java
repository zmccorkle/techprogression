package me.zebmccorkle.techprogression;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class RenderFloatingHead extends RenderLiving {

	public RenderFloatingHead(ModelBase p_i46153_2_,
			float p_i46153_3_) {
		super(Minecraft.getMinecraft().getRenderManager(), p_i46153_2_, p_i46153_3_);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity arg0) {
		return new ResourceLocation(TechProgression.MODID, "textures/entity/FloatingHead.png");
	}

}

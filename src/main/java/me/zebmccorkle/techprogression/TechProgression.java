package me.zebmccorkle.techprogression;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;

@Mod(modid = TechProgression.MODID, version = TechProgression.VERSION)
public class TechProgression {
	public static ToolMaterial mechanicalMaterial;
	public static EnergyNet energyNet;
    public static final String MODID = "techprogression";
    public static final String VERSION = "0.1.0";
    
    @EventHandler
    public void preinit(FMLPreInitializationEvent event) {
    	energyNet = new EnergyNet();
    	TechProgressionEventHandler events = new TechProgressionEventHandler();
    	FMLCommonHandler.instance().bus().register(events);
    	MinecraftForge.EVENT_BUS.register(events);
    	mechanicalMaterial = EnumHelper.addToolMaterial("Mechanical", 2, 300, 30, 4, 10);
    	ItemManager.mainRegistry();
    	createEntity(FloatingHead.class, "FloatingHead", 0x00FF00, 0xFF0000);
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
        System.out.println("DIRT BLOCK >> "+Blocks.dirt.getUnlocalizedName());
        System.out.println("LOCALIZED DIRT BLOCK >> "+Blocks.dirt.getLocalizedName());
        
        RenderingRegistry.registerEntityRenderingHandler(FloatingHead.class, new RenderFloatingHead(new ModelFloatingHead(), 0.5f));
        EntityRegistry.addSpawn(FloatingHead.class, 1000, 1, 5, EnumCreatureType.MONSTER);
        
        GameRegistry.addRecipe(
        		new ItemStack(ItemManager.mechanicalDrill),
        		" II",
        		"SSI",
        		"SS ",
        		'I', new ItemStack(Items.iron_ingot),
        		'S', new ItemStack(Blocks.stone)
        );
        GameRegistry.addRecipe(
        		new ItemStack(ItemManager.diamondMechanicalDrill),
        		"DD",
        		"MD",
        		'D', new ItemStack(Items.diamond),
        		'M', new ItemStack(ItemManager.mechanicalDrill)
        );
        
        if(event.getSide() == Side.CLIENT) {
            RenderItem renderItem = Minecraft.getMinecraft().getRenderItem();
 
            renderItem.getItemModelMesher().register(ItemManager.explosionWand, 0, new ModelResourceLocation(this.MODID + ":" + ItemManager.explosionWand.unlocalizedName, "inventory"));
            renderItem.getItemModelMesher().register(ItemManager.mechanicalDrill, 0, new ModelResourceLocation(this.MODID + ":" + ItemManager.mechanicalDrill.unlocalizedName, "inventory"));
            renderItem.getItemModelMesher().register(ItemManager.diamondMechanicalDrill, 0, new ModelResourceLocation(this.MODID + ":" + ItemManager.diamondMechanicalDrill.unlocalizedName, "inventory"));
        }
    }
    
    public static void createEntity(Class entityClass, String entityName, int solidColor, int spotColor) {
    	int entityId = EntityRegistry.findGlobalUniqueEntityId();
        EntityRegistry.registerGlobalEntityID(entityClass, entityName, entityId);
        EntityList.entityEggs.put(Integer.valueOf(entityId), new EntityList.EntityEggInfo(entityId, solidColor, spotColor));
    }
}

package me.zebmccorkle.techprogression;

import java.text.DecimalFormat;

import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;

public class Thermometer extends Item {
	public static final String unlocalizedName = "Thermometer";
	private boolean debounce = true;
	
	public Thermometer() {
		super();

		this.setUnlocalizedName(TechProgression.MODID + "_" + unlocalizedName);
		this.setCreativeTab(CreativeTabs.tabTools);
	}

	@Override
	public boolean onItemUse(ItemStack par1ItemStack, EntityPlayer par3EntityPlayer, World par2World, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ) {
		//if (debounce) {
			IBlockState blockState = par2World.getBlockState(pos);
			double temp = TechProgression.energyNet.getTemperature(par2World, new BlockPos(pos.getX(), pos.getY(), pos.getZ()));
			par3EntityPlayer.addChatMessage(new ChatComponentText("\u00a7b<Temperature>\u00a7r " + new DecimalFormat("#.##").format(temp) + "\u212a, " + new DecimalFormat("#.##").format(ThermoHelper.kToC(temp)) + "\u2103"));
		//}
		//debounce = !debounce;
		
		return false;
	}
}

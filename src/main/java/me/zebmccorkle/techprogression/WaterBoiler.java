package me.zebmccorkle.techprogression;

import java.util.List;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemBanner;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityBanner;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class WaterBoiler extends BlockContainer implements IHeatProvider {
    public static final PropertyInteger LEVEL = PropertyInteger.create("level", 0, 3);
    private static final String __OBFID = "CL_00000213";
    public static final String unlocalizedName = "WaterBoiler";

    public WaterBoiler()
    {
        super(Material.iron);
        this.setDefaultState(this.blockState.getBaseState().withProperty(LEVEL, Integer.valueOf(0)));
        this.setUnlocalizedName(TechProgression.MODID + "_" + unlocalizedName);
        this.setHardness(0);
    }

    public boolean isOpaqueCube()
    {
        return false;
    }

    public boolean isFullCube()
    {
        return true;
    }

    /**
     * Called When an Entity Collided with the Block
     */
    public void onEntityCollidedWithBlock(World worldIn, BlockPos pos, IBlockState state, Entity entityIn)
    {
        int i = ((Integer)state.getValue(LEVEL)).intValue();
        float f = (float)pos.getY() + (6.0F + (float)(3 * i)) / 16.0F;

        if (!worldIn.isRemote && entityIn.isBurning() && i > 0 && entityIn.getEntityBoundingBox().minY <= (double)f)
        {
            entityIn.extinguish();
            this.setWaterLevel(worldIn, pos, state, i - 1);
        }
    }

    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumFacing side, float hitX, float hitY, float hitZ)
    {
        if (worldIn.isRemote)
        {
            return true;
        }
        else
        {
            ItemStack itemstack = playerIn.inventory.getCurrentItem();

            if (itemstack == null)
            {
                return true;
            }
            else
            {
                int i = ((Integer)state.getValue(LEVEL)).intValue();
                Item item = itemstack.getItem();

                if (item == Items.water_bucket)
                {
                    if (i < 3)
                    {
                        if (!playerIn.capabilities.isCreativeMode)
                        {
                            playerIn.inventory.setInventorySlotContents(playerIn.inventory.currentItem, new ItemStack(Items.bucket));
                        }

                        this.setWaterLevel(worldIn, pos, state, 3);
                        if (getTemperature(new IndividualBlock(worldIn, pos)) > ThermoHelper.cToK(100) && worldIn.getBlockState(new BlockPos(pos.getX(), pos.getY()+1, pos.getZ())) == Blocks.air.getDefaultState()) {
                        	worldIn.setBlockState(new BlockPos(pos.getX(), pos.getY()+1, pos.getZ()), ItemManager.steamBlock.getDefaultState());
                        }
                    }

                    return true;
                }
                else
                {
                    ItemStack itemstack1;

                    if (item == Items.glass_bottle)
                    {
                        if (i > 0)
                        {
                            if (!playerIn.capabilities.isCreativeMode)
                            {
                                itemstack1 = new ItemStack(Items.potionitem, 1, 0);

                                if (!playerIn.inventory.addItemStackToInventory(itemstack1))
                                {
                                    worldIn.spawnEntityInWorld(new EntityItem(worldIn, (double)pos.getX() + 0.5D, (double)pos.getY() + 1.5D, (double)pos.getZ() + 0.5D, itemstack1));
                                }
                                else if (playerIn instanceof EntityPlayerMP)
                                {
                                    ((EntityPlayerMP)playerIn).sendContainerToPlayer(playerIn.inventoryContainer);
                                }

                                --itemstack.stackSize;

                                if (itemstack.stackSize <= 0)
                                {
                                    playerIn.inventory.setInventorySlotContents(playerIn.inventory.currentItem, (ItemStack)null);
                                }
                            }

                            this.setWaterLevel(worldIn, pos, state, i - 1);
                        }

                        return true;
                    }
                    else
                    {
                        if (i > 0 && item instanceof ItemArmor)
                        {
                            ItemArmor itemarmor = (ItemArmor)item;

                            if (itemarmor.getArmorMaterial() == ItemArmor.ArmorMaterial.LEATHER && itemarmor.hasColor(itemstack))
                            {
                                itemarmor.removeColor(itemstack);
                                this.setWaterLevel(worldIn, pos, state, i - 1);
                                return true;
                            }
                        }

                        if (i > 0 && item instanceof ItemBanner && TileEntityBanner.getPatterns(itemstack) > 0)
                        {
                            itemstack1 = itemstack.copy();
                            itemstack1.stackSize = 1;
                            TileEntityBanner.removeBannerData(itemstack1);

                            if (itemstack.stackSize <= 1 && !playerIn.capabilities.isCreativeMode)
                            {
                                playerIn.inventory.setInventorySlotContents(playerIn.inventory.currentItem, itemstack1);
                            }
                            else
                            {
                                if (!playerIn.inventory.addItemStackToInventory(itemstack1))
                                {
                                    worldIn.spawnEntityInWorld(new EntityItem(worldIn, (double)pos.getX() + 0.5D, (double)pos.getY() + 1.5D, (double)pos.getZ() + 0.5D, itemstack1));
                                }
                                else if (playerIn instanceof EntityPlayerMP)
                                {
                                    ((EntityPlayerMP)playerIn).sendContainerToPlayer(playerIn.inventoryContainer);
                                }

                                if (!playerIn.capabilities.isCreativeMode)
                                {
                                    --itemstack.stackSize;
                                }
                            }

                            if (!playerIn.capabilities.isCreativeMode)
                            {
                                this.setWaterLevel(worldIn, pos, state, i - 1);
                            }

                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
        }
    }

    public void setWaterLevel(World worldIn, BlockPos pos, IBlockState state, int level)
    {
        worldIn.setBlockState(pos, state.withProperty(LEVEL, Integer.valueOf(MathHelper.clamp_int(level, 0, 3))), 2);
        worldIn.updateComparatorOutputLevel(pos, this);
    }

    /**
     * Called similar to random ticks, but only when it is raining.
     */
    public void fillWithRain(World worldIn, BlockPos pos)
    {
        if (worldIn.rand.nextInt(20) == 1)
        {
            IBlockState iblockstate = worldIn.getBlockState(pos);

            if (((Integer)iblockstate.getValue(LEVEL)).intValue() < 3)
            {
                worldIn.setBlockState(pos, iblockstate.cycleProperty(LEVEL), 2);
            }
        }
    }

    /**
     * Get the Item that this Block should drop when harvested.
     *  
     * @param fortune the level of the Fortune enchantment on the player's tool
     */
    public Item getItemDropped(IBlockState state, Random rand, int fortune)
    {
        return Item.getItemFromBlock(this);
    }

    @SideOnly(Side.CLIENT)
    public Item getItem(World worldIn, BlockPos pos)
    {
        return Item.getItemFromBlock(this);
    }

    public boolean hasComparatorInputOverride()
    {
        return true;
    }

    public int getComparatorInputOverride(World worldIn, BlockPos pos)
    {
        return ((Integer)worldIn.getBlockState(pos).getValue(LEVEL)).intValue();
    }

    /**
     * Convert the given metadata into a BlockState for this Block
     */
    public IBlockState getStateFromMeta(int meta)
    {
        return this.getDefaultState().withProperty(LEVEL, Integer.valueOf(meta));
    }

    /**
     * Convert the BlockState into the correct metadata value
     */
    public int getMetaFromState(IBlockState state)
    {
        return ((Integer)state.getValue(LEVEL)).intValue();
    }

    protected BlockState createBlockState()
    {
        return new BlockState(this, new IProperty[] {LEVEL});
    }

	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new WaterBoilerTileEntity();
	}
	
	@Override
	public double getTemperature(IndividualBlock block) {
		IndividualBlock below = new IndividualBlock(block.world, new BlockPos(block.pos.getX(), block.pos.getY()-1, block.pos.getZ()));
		return TechProgression.energyNet.getTemperature(below) < TechProgression.energyNet.ROOM_TEMPERATURE ? TechProgression.energyNet.ROOM_TEMPERATURE : TechProgression.energyNet.getTemperature(below) * TechProgression.energyNet.RESISTANCE_MULT;
	}
}
package me.zebmccorkle.techprogression;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTool;
import net.minecraft.block.Block;

public class MechanicalDrill extends Drill {
	public static String unlocalizedName = "MechanicalDrill";
	
	protected MechanicalDrill(ToolMaterial material) {
		super(material);
		
		this.setUnlocalizedName(TechProgression.MODID + "_" + this.unlocalizedName);
		this.setMaxDamage(132);
		this.setCreativeTab(CreativeTabs.tabTools);
	}
}
